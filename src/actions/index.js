import axios from "axios";
const artistURL = "https://www.theaudiodb.com/api/v1/json/1/search.php"
const albumURL = "https://theaudiodb.com/api/v1/json/1/album.php"

export const searchActions = {
    FetchArtist: 'FETCH_ARTIST',
    FetchAlbum: 'FETCH_ALBUM'
}

export const actFetchArtist = (artist) => async dispatch => {
    let artistInfo = []
    axios.get(`${artistURL}?s=${artist}`)
        .then(res => {
            artistInfo = res.data.artists[0]
            dispatch({
                type: searchActions.FetchArtist,
                payload: artistInfo
            })
        })
        .catch(err => {
            alert("Artist Not Found")
            console.log(err)
        })
}

export const actFetchAlbum = (id) => async dispatch => {
    let albumInfo = []
    axios.get(`${albumURL}?i=${id}`)
        .then(res => {
            albumInfo = res.data
            dispatch({
                type: searchActions.FetchAlbum,
                payload: albumInfo
            })
        })
        .catch(err => {
            console.log(err)
        })
}

