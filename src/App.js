import './app.css'
import {BrowserRouter, Route} from "react-router-dom";
import Header from "./components/Header";
import Main from "./components/Main";
import About from "./components/About";

function App() {
    return (
        <div className="layout">
            <BrowserRouter>
                <Route component={Header}/>
                <div className="mainContent">
                    <Route path="/" exact component={Main}/>
                    <Route path="/About" exact component={About}/>
                </div>
            </BrowserRouter>
        </div>
    )
}

export default App;
