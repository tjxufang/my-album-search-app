import {combineReducers} from "redux"
import {searchActions} from "../actions";

const initState = {
    artistInfo: undefined,
    albumInfo: undefined
}

const fetchArtistReducer = (state = initState, action) => {
    const {type, payload} = action
    switch (type) {
        case (searchActions.FetchArtist):
            return {...state, artistInfo: payload}
        case (searchActions.FetchAlbum):
            return {...state, albumInfo: payload}
        default:
            return state
    }
}

export default combineReducers({
    fetchArtistReducer,
})