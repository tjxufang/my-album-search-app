import React from 'react';
import logo from "../assets/logo192.png";
import './header.css'
import {Link, useLocation} from "react-router-dom";

const Header = () => {
    const location = useLocation()
    return (
        <div className="headerContainer">
            <div className="header">
                <div className="logo">
                    <Link to={'/'}>
                        <img src={logo} alt="logo" width='45px'/>
                        <h2>Search by Your Favourite Artist</h2>
                    </Link>
                </div>
                {location.pathname === "/about" ?
                    <div className="about">
                        <p>Thank you for visiting!</p>
                    </div>
                    :
                    <div className="about">
                        <Link to={"/about"}>
                            <h3>About Me</h3>
                        </Link>
                    </div>
                }
            </div>
        </div>
    )

}


export default Header
