import React from 'react';
import './main.css'
import {useHistory} from "react-router-dom";

const About = () => {
    const history = useHistory()

    return (
        <div className="container" style={{padding: '25px'}}>
            <h2>About Me</h2>
            <div className="textField">
                <p>My name is Fang Xu who owns hands-on experience on React.js and all of its related frameworks and technologies. I am a solution-driven developer who contributes high quality code and have joined multiple live projects under an agile environment with customers in different industries with different needs and requirements.</p>
                <p>I am a fast learner to new languages as well as a quick adapter. Last year, I completely my certificate in business at Smith School of Business at Queen’s University with a great interest in the financial/banking industry.</p>
                <p>Looking forward to joining the team and provide value to the company!</p>
                <p>Fang Xu</p>
            </div>

            <button onClick={() => history.push('/')}>Go back</button>
        </div>
    );
}

export default About;