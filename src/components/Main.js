import React from 'react';
import './main.css'
import SearchBar from "./search/SearchBar";
import SearchResult from "./search/SearchResult";

const Main = () => {
    return (
        <div>
            <div className="container">
                <SearchBar/>
            </div>
            <div className="container">
                <SearchResult/>
            </div>
        </div>
    );
}

export default Main;