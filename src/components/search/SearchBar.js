import React, {useState} from 'react';
import {actFetchAlbum, actFetchArtist} from "../../actions";
import {connect} from "react-redux";
import './searchBar.css'

const SearchBar = ({actFetchArtist}) => {
    const [input, setInput] = useState()

    const handleType = (evt) =>{
        setInput(evt.target.value.trimLeft())
    }
    const handleSearch = (evt) =>{
        evt.preventDefault()
        actFetchArtist(input)
    }

    return (
        <div>
            <h1>Album Search Engine</h1>
            <div className="searchBar">
                <form onSubmit={handleSearch}>
                    <input
                        type="text"
                        placeholder="  Artist Name..."
                        onChange={handleType}
                    />
                    <button type="submit">Search</button>
                </form>
            </div>

        </div>
    );
}


const mapDispatchToProps = {actFetchArtist, actFetchAlbum}


export default connect(null, mapDispatchToProps)(SearchBar)