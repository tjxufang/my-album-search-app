import React from 'react';
import './filter.css'

const Filter = (props) => {
    const {handleClick} = props;

    return (
        <div className="filter">
            <h2>Show Albums from:</h2>
            <button value="all" onClick={handleClick}>Show All</button>
            <button value="0010" onClick={handleClick}>2000-2010</button>
            <button value="b400" onClick={handleClick}>Before 2000</button>
        </div>
    );
}

export default Filter;