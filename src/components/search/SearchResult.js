import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import "./searchResult.css"
import {actFetchAlbum} from "../../actions";
import Filter from "./Filter";

const SearchResult = ({artistInfo, albumInfo, actFetchAlbum}) => {
    const artistId = artistInfo?.idArtist
    useEffect(
        () => {
            actFetchAlbum(artistId)
        }, [actFetchAlbum, artistId])

    const [filter, setFilter] = useState('all')

    const album0010 = albumInfo?.filter(album => (parseInt(album.intYearReleased) < 2010 && parseInt(album.intYearReleased) > 2000))
    const albumb400 = albumInfo?.filter(album => parseInt(album.intYearReleased) < 2000)

    const handleClick = (evt) => {
        setFilter(evt.target.value)
    }

    const renderAlbum = () => {
        let albums
        if (filter === '0010') {
            albums = album0010
        } else if (filter === 'all') {
            albums = albumInfo
        } else if (filter === 'b400'){
            albums = albumb400
        }
        return (
            albums?.map((album, i) => {
                return (
                    <div key={i} className="albumCard">
                        <img src={album.strAlbumThumb} alt="Album Thumbnail" style={{width: '100%'}}/>
                        <div className="albumDescription">
                            <h4><b>{album.strAlbum}</b></h4>
                            <p>{album.intYearReleased}</p>
                            <p>{album.strGenre}</p>
                            <p>{album.strLabel}</p>
                        </div>
                    </div>
                )
            })
        )
    }

    return (
        <div>
            {artistInfo ?
                <div className="result">
                    <Filter handleClick={handleClick}/>
                    <div className="artistCard">
                        <img src={artistInfo?.strArtistThumb} alt="Artist Thumbnail" style={{width: '100%'}}/>
                        <div className="artistDescription">
                            <h3>Artist Name: <b>{artistInfo?.strArtist}</b></h3>
                            <p>Gender: {artistInfo?.strGender}</p>
                            <p>Label: {artistInfo?.strLabel}</p>
                            <p>Genre: {artistInfo?.strGenre}</p>
                            <p>Country: {artistInfo?.strCountry}</p>
                        </div>
                    </div>
                    {albumInfo ? renderAlbum() : <h2>Loading Albums...</h2>}
                </div>
                :
                <div>Please enter a name</div>
            }
        </div>
    );
}

const mapStateToProps = state => {
    return {
        artistInfo: state.fetchArtistReducer.artistInfo,
        albumInfo: state.fetchArtistReducer.albumInfo?.album,
    }
}

export default connect(mapStateToProps,
    {
        actFetchAlbum
    }
)(SearchResult)